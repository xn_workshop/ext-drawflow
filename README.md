# Drawflow Editor extension for laravel-admin

This is a `laravel-admin` extension that integrates [Drawflow Editor](https://github.com/jerosoler/Drawflow) into `laravel-admin`.

[DEMO](http://demo.laravel-admin.org/editors/json) Login using `admin/admin`


## Installation

```bash
composer require xn/drawflow-editor
php artisan vendor:publish --tag=laravel-admin-drawflow-editor
```

## Update
```bash
composer require xn/drawflow-editor
php artisan vendor:publish --tag=laravel-admin-drawflow-editor --force
```

## Configuration

In the `extensions` section of the `config/admin.php` file, add some configuration that belongs to this extension.
```php
'extensions' => [
    'drawflow-editor' => [
        // set to false if you want to disable this extension
        'enable' => true,
        'config' => [
// 
        ],
    ]
]
```

More configurations can be found at [Drawflow Editor](https://github.com/jerosoler/Drawflow).

## Usage

Use it in the form form:
```php
$form->drawflow('content');
```

## More resources

[Awesome Laravel-admin](https://github.com/jxlwqq/awesome-laravel-admin)

## License

Licensed under [The MIT License](LICENSE).
