<?php

namespace Xn\DrawflowEditor;

use Xn\Admin\Admin;
use Xn\Admin\Form;
use Illuminate\Support\ServiceProvider;

class DrawflowEditorServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function boot(DrawflowEditorExtension $extension)
    {
        if (! DrawflowEditorExtension::boot()) {
            return ;
        }

        $this->loadViewsFrom(resource_path('views')."/vendor/xn/drawflow-editor/views", $extension->name);

        $this->registerPublishing($extension);

        Admin::booting(function () {
            Form::extend('drawflow', Editor::class);
        });
    }

    /**
     * Register the package's publishable resources.
     *
     * @return void
     */
    protected function registerPublishing(DrawflowEditorExtension $extension)
    {
        if ($this->app->runningInConsole()) {
            $views = $extension->views();
            $assets = $extension->assets();
            $this->publishes(
                [
                    $views => resource_path('views')."/vendor/xn/drawflow-editor/views",
                    $assets => public_path('vendor/laravel-admin-ext/drawflow-editor')
                ], 
                $extension->name
            );
        }
    }
}
