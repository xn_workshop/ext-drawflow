<?php

namespace Xn\DrawflowEditor;

use Xn\Admin\Form\Field;
use Xn\Admin\Form\NestedForm;

class Editor extends Field
{
    protected $view = 'laravel-admin-drawflow-editor::editor';

    protected static $css = [
        '/vendor/laravel-admin-ext/drawflow-editor/drawflow/dist/drawflow.min.css',
    ];

    protected static $js = [
        '/vendor/laravel-admin-ext/drawflow-editor/drawflow/dist/drawflow.min.js',
    ];

    public function render()
    {

        $json = old($this->column, $this->value());

        $this->value = $json;

        $options = json_encode(config('admin.extensions.drawflow-editor.config'));

        $name = $this->variables()["name"];
        $_name = str_replace(['[',']'], '_',$name);
        $defaultKey = NestedForm::DEFAULT_KEY_NAME;
        $this->script = <<<EOT

$(function(){
    // create the editor
    var name="$name";
    var _name="$_name";
    try {
        name="$name".replace(/{$defaultKey}/g, index);
        _name="$_name".replace(/{$defaultKey}/g, index);
    }catch(e){
    }

    // Initialize Drawflow
    editor = new Drawflow(document.getElementById(_name));
    editor.reroute = true;
    editor.reroute_fix_curvature = true;
    editor.force_first_input = false;
    editor.start(function(){
        url = '/vendor/laravel-admin-ext/drawflow-editor/drawflow/drawflow-events.js';
        var script = document.createElement('script');
        script.onload = function () {
            console.log('custom drawflow loaded');
        };
        script.src = url;
        document.head.appendChild(script);
    });
    if ('$this->value') {
        editor.import($this->value);
    }
    $('form').on('submit', function(){
        $('input[name="'+name+'"]').val(JSON.stringify(editor.export()));
        return true;
    });
});

EOT;

        return parent::render();
    }
}
