<?php

namespace Xn\DrawflowEditor;

use Xn\Admin\Extension;

class DrawflowEditorExtension extends Extension
{
    public $name = 'laravel-admin-drawflow-editor';

    public $views = __DIR__.'/../resources/views';

    public $assets = __DIR__.'/../resources/assets';
}
